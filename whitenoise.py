import numpy
from PIL import Image

output = "output.gif"

width = 128
height = width
num_frames = 4
sigma = 64



frames = []
for i in range(num_frames):
    frames.append(Image.effect_noise((width, height), sigma))

gif = frames[0]

gif.save(fp=output, format="GIF", append_images=frames[1:], save_all=True, duration=200, loop=0)
